## Variables

### Role defaults

| Variable                  | Type              | Default     | Description |
| ------------------------- | ----------------- | ----------- | ----------- |
| `lego__path`              | `str`             | `/etc/lego` | Path where lego should put certificates. |
| `lego__desec_ttl`         | `int`             | `3600`      | TTL value for TXT records with secrets (minimum 3600). |

### Role variables

| Variable                  | Type              | Description |
| ------------------------- | ----------------- | ----------- |
| `lego__certs`             | `List[List[str]]` | List of certificates which are lists of FQDNs, beginning with the certificate name. |
| `lego__letsencrypt_email` | `str`             | Email to use for the LetsEncrypt account. |
| `lego__desec_token`       | `str`             | Token to use with the desec API. |

